import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.PersistentSet
import kotlinx.collections.immutable.persistentMapOf
import kotlinx.collections.immutable.persistentSetOf
import kotlin.random.Random

typealias GenEnvironment<V> = PersistentMap<V, Int>

fun interface Interpretable<V, E : GenEnvironment<V>> {
    fun interpret(environment: E): Int
}

fun interface Uniqueable {
    fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>>
}

interface RVar : Uniqueable, Interpretable<RVarEnvironment>


data class Counts(
    val highest: PersistentMap<String, Int>,
    val closest: PersistentMap<String, Int>
)

interface Integer<E> : Uniqueable, Interpretable<E> {
    val value: Int

    override fun interpret(environment: E): Int {
        return value
    }

    override fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>> {
        return Pair(runique.Integer(value), counts.highest)
    }
}

data class RVarInteger(override val value: Int) : Integer<RVarEnvironment>, RVar

interface Read<E> : Uniqueable, Interpretable<E> {
    override fun interpret(environment: E): Int {
        return readLine()!!.toInt()
    }

    override fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>> {
        return Pair(runique.Read, counts.highest)
    }
}

object RVarRead : Read<RVarEnvironment>, RVar

interface Negate<C : Interpretable<E>, E> : Interpretable<E> {
    val child: C

    override fun interpret(environment: E): Int {
        return 0 - child.interpret(environment)
    }
}

data class RVarNegate(override val child: RVar) : Negate<RVar, RVarEnvironment>, RVar {
    override fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>> {
        val (newE, newCounts) = child.uniqueify(counts)
        return Pair(runique.Negate(newE), newCounts)
    }
}

interface Plus<C : Interpretable<E>, E> : Interpretable<E> {
    val left: C
    val right: C

    override fun interpret(environment: E): Int {
        return left.interpret(environment) + right.interpret(environment)
    }
}

data class RVarPlus(override val left: RVar, override val right: RVar) : Plus<RVar, RVarEnvironment>, RVar {
    override fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>> {
        val (newLeft, leftCounts) = left.uniqueify(counts)
        val (newRight, rightCounts) = right.uniqueify(Counts(leftCounts, counts.closest))

        return Pair(runique.Plus(newLeft, newRight), rightCounts)
    }
}

data class RVarVar(val name: String) : RVar {
    override fun interpret(environment: RVarEnvironment): Int {
        val v = environment[this]
        if (v == null) {
            error("Unable to find variable $this in environment $environment.")
        } else {
            return v
        }
    }

    override fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>> {
        return Pair(runique.Var(name, counts.closest[name]!!), counts.highest)
    }
}

interface Let<VarType, BoundType, InType, E> : Interpretable<E> {

    val boundVar: VarType
    val boundExpression: BoundType
    val inExpression: InType


    override fun interpret(environment: E): Int {
        val newEnv = environment.put(Var(varName), varExpression.interpret(environment))
        return inExpression.interpret(newEnv)
    }

    override fun checkUniqueSymbols(environment: PersistentSet<String>): Boolean {
        if (varName in environment) {
            error("Variable $varName already exists in environment $environment.")
        } else {
            return inExpression.checkUniqueSymbols(environment.add(varName))
        }
    }

        val count = (counts.highest[varName]?.plus(1)) ?: 0
        val newHighest = counts.highest.put(varName, count)
        val newClosest = counts.closest.put(varName, count)

        val (newVarE, varHighest) = varExpression.uniqueify(Counts(newHighest, counts.closest))
        val (newInE, inHighest) = inExpression.uniqueify(Counts(varHighest, newClosest))

        return Pair(
            runique.Let(
                runique.Var(varName, count),
                newVarE,
                newInE
            ), inHighest
        )
    }
}

data class Top(val e: Expression) : Program {
    override fun interpret(environment: Environment): Int {
        return e.interpret(environment)
    }

    fun interpret(): Int {
        return e.interpret(persistentMapOf())
    }

    companion object {
        fun random(maxDepth: Int): Top {
            return Top(randomExpression(persistentMapOf(), maxDepth))
        }
    }

    override fun checkUniqueSymbols(environment: PersistentSet<String>): Boolean {
        return e.checkUniqueSymbols(environment)
    }

    fun checkUniqueSymbols(): Boolean {
        return e.checkUniqueSymbols(persistentSetOf())
    }

    fun uniqueify(): runique.Top {
        val (newE, _) = e.uniqueify(Counts(persistentMapOf(), persistentMapOf()))
        return runique.Top(newE)
    }
}

//fun Int.toVar(): String {
//    fun loop(s: String, x : Int): String {
//        return if (x < 26) {
//            (s + (x + 97).toChar()).reversed();
//        } else {
//            val c = ((x % 26) + 97).toChar();
//            val r = (x / 26) - 1;
//            loop(s + c, r)
//        }
//    }
//
//    return loop("", this)
//}
//
//fun Environment.genSym(): String {
//    return this.size.toVar()
//}

fun randomLeaf(environment: Environment): Expression {
    return if (environment.isEmpty()) {
        Integer(Random.nextInt(1000))
    } else {
        if (Random.nextBoolean()) {
            Integer(Random.nextInt(1000))
        } else {
            environment.keys.toList()[Random.nextInt(environment.size)]
        }
    }
}

fun randomExpression(environment: Environment, maxDepth: Int): Expression {
    if (maxDepth == 0) {
        return randomLeaf(environment)
    } else {
        when (Random.nextInt(4)) {
            0 -> return randomLeaf(environment)
            1 -> return Negate(randomExpression(environment, maxDepth - 1))
            2 -> return Plus(
                randomExpression(environment, maxDepth - 1),
                randomExpression(environment, maxDepth - 1)
            )
            3 -> {
                val sym = listOf("a", "b", "c")[Random.nextInt(3)]
                //val sym = environment.genSym()
                val newEnv = environment.put(Var(sym), 0)
                return Let(sym, randomExpression(environment, maxDepth - 1), randomExpression(newEnv, maxDepth - 1))
            }
        }
        return Integer(0)
    }
}


fun main() {
    val p = Top(
        Let(
            "x",
            Integer(10),
            Let(
                "x",
                Var("x"),
                Var("x")
            )
        )
    )

    println("${p.interpret()}")

    val u = p.uniqueify()
    println(u)
    println("${u.interpret()}")
    println("${u.checkUniqueSymbols()}")
}