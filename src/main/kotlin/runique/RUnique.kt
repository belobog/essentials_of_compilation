package runique

import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.PersistentSet
import kotlinx.collections.immutable.persistentMapOf
import kotlinx.collections.immutable.persistentSetOf


typealias Environment = PersistentMap<Var, Int>

interface Program {
    fun interpret(environment: Environment): Int
}

interface Expression : Program {
    fun removeComplex(count: Int): Pair<anf.Expression, Int>

    fun checkUniqueSymbols(): PersistentSet<Var>
}

interface Atomic : Expression {
    fun anf(): anf.Atomic
    override fun removeComplex(count: Int): Pair<anf.Expression, Int> = Pair(anf(), count)
}

fun Int.tmp(): anf.Var = anf.Var("anfTemp", this)

data class Integer(val value: Int) : Atomic {
    override fun interpret(environment: Environment): Int {
        return value
    }

    override fun checkUniqueSymbols(): PersistentSet<Var> = persistentSetOf()

    override fun anf(): anf.Atomic {
        return anf.Integer(value)
    }
}

object Read : Expression {
    override fun interpret(environment: Environment): Int {
        return readLine()!!.toInt()
    }

    override fun checkUniqueSymbols(): PersistentSet<Var> = persistentSetOf()

    override fun removeComplex(count: Int): Pair<anf.Expression, Int> {
        return Pair(
            anf.Read,
            count
        )
    }
}

data class Negate(val expression: Expression) : Expression {
    override fun interpret(environment: Environment): Int {
        return 0 - expression.interpret(environment)
    }

    override fun checkUniqueSymbols(): PersistentSet<Var> {
        return expression.checkUniqueSymbols()
    }

    override fun removeComplex(count: Int): Pair<anf.Expression, Int> {
        return when (expression) {
            is Atomic -> Pair(anf.Negate(expression.anf()), count)
            else -> {
                val boundVar = count.tmp()
                val (anfE, newCount) = expression.removeComplex(count + 1)

                Pair(anf.Let(
                    boundVar,
                    anfE,
                    anf.Negate(boundVar)
                ), newCount)
            }
        }
    }
}

data class Plus(val left: Expression, val right: Expression) : Expression {
    override fun interpret(environment: Environment): Int {
        return left.interpret(environment) + right.interpret(environment)
    }

    override fun checkUniqueSymbols(): PersistentSet<Var> {
        val leftVars = left.checkUniqueSymbols()
        val rightVars = right.checkUniqueSymbols()

        assert(leftVars.intersect(rightVars).isEmpty())

        return leftVars.addAll(rightVars)
    }

    override fun removeComplex(count: Int): Pair<anf.Expression, Int> {
        return when (left) {
            is Atomic -> {
                when (right) {
                    is Atomic -> Pair(anf.Plus(left.anf(), right.anf()), count)
                    else -> {
                        val rightBound = count.tmp()
                        val (anfRight, rightCount) = right.removeComplex(count + 1)

                        Pair(anf.Let(
                            rightBound,
                            anfRight,
                            anf.Plus(left.anf(), rightBound)
                        ), rightCount)
                    }
                }
            }
            else -> {
                when (right) {
                    is Atomic -> {
                        val leftBound = count.tmp()
                        val (anfLeft, leftCount) = left.removeComplex(count + 1)
                        Pair(anf.Let(
                            leftBound,
                            anfLeft,
                            anf.Plus(leftBound, right.anf())
                        ), leftCount)
                    }
                    else -> {
                        val leftBound = count.tmp()
                        val (anfLeft, leftCount) = left.removeComplex(count + 1)
                        val rightBound = leftCount.tmp()
                        val (anfRight, rightCount) = right.removeComplex(leftCount + 1)


                        Pair(anf.Let(
                            leftBound,
                            anfLeft,
                            anf.Let(
                                rightBound,
                                anfRight,
                                anf.Plus(leftBound, rightBound)
                            )
                        ), rightCount)
                    }
                }
            }
        }
    }
}

data class Var(val name: String, val count: Int) : Atomic {
    override fun interpret(environment: Environment): Int {
        val v = environment[this]
        if (v == null) {
            error("Unable to find variable $this in environment $environment.")
        } else {
            return v
        }
    }

    override fun checkUniqueSymbols(): PersistentSet<Var> = persistentSetOf()

    override fun anf(): anf.Var {
        return anf.Var(name, count)
    }
}

data class Let(val boundVar: Var, val varExpression: Expression, val inExpression: Expression) : Expression {
    override fun interpret(environment: Environment): Int {
        val newEnv = environment.put(boundVar, varExpression.interpret(environment))
        return inExpression.interpret(newEnv)
    }

    override fun checkUniqueSymbols(): PersistentSet<Var> {
        val varVars = varExpression.checkUniqueSymbols()
        val inVars = inExpression.checkUniqueSymbols()

        assert(varVars.intersect(inVars).isEmpty())

        val allVars = varVars.addAll(inVars)

        assert(boundVar !in allVars)

        return allVars.add(boundVar)
    }

    override fun removeComplex(count: Int): Pair<anf.Expression, Int> {
        val (anfVar, varCount) = varExpression.removeComplex(count)
        val (anfIn, inCount) = inExpression.removeComplex(varCount)

        return Pair(anf.Let(
            boundVar.anf(),
            anfVar,
            anfIn
        ), inCount)
    }
}

data class Top(val e: Expression) : Program {
    override fun interpret(environment: Environment): Int {
        return e.interpret(environment)
    }

    fun interpret(): Int {
        return e.interpret(persistentMapOf())
    }

    fun checkUniqueSymbols(): Boolean {
        val allVars = e.checkUniqueSymbols()
        return true
    }

    fun removeComplex() : anf.Top {
        val (anfE, _) = e.removeComplex(0)
        return anf.Top(anfE)
    }
}

fun main() {
    for (i in 0 until 1000000) {
        val p = rvar.Top.random(20)
//        println(p)
        val varValue = p.interpret()
        val u = p.uniqueify()
//        println(u)
        val uniqueValue = u.interpret()
        println("$i $varValue $uniqueValue")
        assert(u.checkUniqueSymbols())
        assert(varValue == uniqueValue)
    }
}