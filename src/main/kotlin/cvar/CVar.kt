package cvar

import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.persistentMapOf


typealias Environment = PersistentMap<Var, Int>

interface Node {
    fun interpret(environment: Environment): Int
}

interface Expression : Node

interface Atomic : Expression

interface Block : Node

data class Integer(val value: Int) : Atomic {
    override fun interpret(environment: Environment): Int {
        return value
    }
}

data class Var(val name: String, val count: Int) : Atomic {
    override fun interpret(environment: Environment): Int {
        val v = environment[this]
        if (v == null) {
            error("Unable to find variable $this in environment $environment.")
        } else {
            return v
        }
    }
}

object Read : Expression {
    override fun interpret(environment: Environment): Int {
        return readLine()!!.toInt()
    }
}

data class Negate(val expression: Atomic) : Expression {
    override fun interpret(environment: Environment): Int {
        return 0 - expression.interpret(environment)
    }
}

data class Plus(val left: Atomic, val right: Atomic) : Expression {
    override fun interpret(environment: Environment): Int {
        return left.interpret(environment) + right.interpret(environment)
    }
}

data class Assign(val boundVar: Var, val boundTo: Expression) {
    fun bind(environment: Environment): Environment {
        return environment.put(boundVar, boundTo.interpret(environment))
    }
}

data class Return(val expression: Expression) : Block {
    override fun interpret(environment: Environment): Int {
        return expression.interpret(environment)
    }
}

data class Sequence(val assign: Assign, val next: Block) : Block {
    override fun interpret(environment: Environment): Int {
        return next.interpret(assign.bind(environment))
    }
}

data class Top(val block: Block) {
    fun interpret(): Int {
        return block.interpret(persistentMapOf())
    }
}

fun main() {
    for (i in 0 until 1000000) {
        val p = rvar.Top.random(20);
//        println(p)
        val varValue = p.interpret()
        val c = p.uniqueify().removeComplex().explicateControl()
//        println(c)
        val cValue = c.interpret()
        println("$i $varValue $cValue")
        assert(varValue == cValue)
    }
}