package xes


enum class Register {
    Rsp, Rbp, Rax, Rbx, Rcx, Rdx, Rsi, Rdi,
    R8, R9, R10, R11, R12, R13, R14, R15
}


interface Test

data class Box<T>(val t : T)

fun Box<Int>.plusOne() : Int {
    return t + 1
}

fun main() {
    val b = Box(1)
    b.plusOne()
}