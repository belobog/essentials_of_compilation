package rvar

import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.PersistentSet
import kotlinx.collections.immutable.persistentMapOf
import kotlinx.collections.immutable.persistentSetOf
import kotlin.random.Random


typealias Environment = PersistentMap<Var, Int>

data class Counts(val highest: PersistentMap<String, Int>,
                  val closest: PersistentMap<String, Int>)

interface Program {
    fun interpret(environment: Environment): Int
    fun checkUniqueSymbols(environment: PersistentSet<String>): Boolean
}

interface Expression : Program {
    fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>>
}


data class Integer(val value: Int) : Expression {
    override fun interpret(environment: Environment): Int {
        return value
    }

    override fun checkUniqueSymbols(environment: PersistentSet<String>): Boolean = true

    override fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>> {
        return Pair(runique.Integer(value), counts.highest)
    }
}

object Read : Expression {
    override fun interpret(environment: Environment): Int {
        return readLine()!!.toInt()
    }

    override fun checkUniqueSymbols(environment: PersistentSet<String>): Boolean = true

    override fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>> {
        return Pair(runique.Read, counts.highest)
    }
}

data class Negate(val expression: Expression) : Expression {
    override fun interpret(environment: Environment): Int {
        return 0 - expression.interpret(environment)
    }

    override fun checkUniqueSymbols(environment: PersistentSet<String>): Boolean {
        return expression.checkUniqueSymbols(environment)
    }

    override fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>> {
        val (newE, newCounts) = expression.uniqueify(counts)
        return Pair(runique.Negate(newE), newCounts)
    }
}

data class Plus(val left: Expression, val right: Expression) : Expression {
    override fun interpret(environment: Environment): Int {
        return left.interpret(environment) + right.interpret(environment)
    }

    override fun checkUniqueSymbols(environment: PersistentSet<String>): Boolean {
        return left.checkUniqueSymbols(environment) && right.checkUniqueSymbols(environment)
    }

    override fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>> {
        val (newLeft, leftCounts) = left.uniqueify(counts)
        val (newRight, rightCounts) = right.uniqueify(Counts(leftCounts, counts.closest))

        return Pair(runique.Plus(newLeft, newRight), rightCounts)
    }
}

data class Var(val name: String) : Expression {
    override fun interpret(environment: Environment): Int {
        val v = environment[this]
        if (v == null) {
            error("Unable to find variable $this in environment $environment.")
        } else {
            return v
        }
    }

    override fun checkUniqueSymbols(environment: PersistentSet<String>): Boolean = true

    override fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>> {
        return Pair(runique.Var(name, counts.closest[name]!!), counts.highest)
    }
}

data class Let(val varName: String, val varExpression: Expression, val inExpression: Expression) : Expression {
    override fun interpret(environment: Environment): Int {
        val newEnv = environment.put(Var(varName), varExpression.interpret(environment))
        return inExpression.interpret(newEnv)
    }

    override fun checkUniqueSymbols(environment: PersistentSet<String>): Boolean {
        if (varName in environment) {
            error("Variable $varName already exists in environment $environment.")
        } else {
            return inExpression.checkUniqueSymbols(environment.add(varName))
        }
    }

    override fun uniqueify(counts: Counts): Pair<runique.Expression, PersistentMap<String, Int>> {
        val count = (counts.highest[varName]?.plus(1)) ?: 0
        val newHighest = counts.highest.put(varName, count)
        val newClosest = counts.closest.put(varName, count)

        val (newVarE, varHighest) = varExpression.uniqueify(Counts(newHighest, counts.closest))
        val (newInE, inHighest) = inExpression.uniqueify(Counts(varHighest, newClosest))

        return Pair(runique.Let(
            runique.Var(varName, count),
            newVarE,
            newInE
        ), inHighest)
    }
}

data class Top(val e: Expression) : Program {
    override fun interpret(environment: Environment): Int {
        return e.interpret(environment)
    }

    fun interpret(): Int {
        return e.interpret(persistentMapOf())
    }

    companion object {
        fun random(maxDepth: Int): Top {
            return Top(randomExpression(persistentMapOf(), maxDepth))
        }
    }

    override fun checkUniqueSymbols(environment: PersistentSet<String>): Boolean {
        return e.checkUniqueSymbols(environment)
    }

    fun checkUniqueSymbols(): Boolean {
        return e.checkUniqueSymbols(persistentSetOf())
    }

    fun uniqueify() : runique.Top {
        val (newE, _) = e.uniqueify(Counts(persistentMapOf(), persistentMapOf()))
        return runique.Top(newE)
    }
}

//fun Int.toVar(): String {
//    fun loop(s: String, x : Int): String {
//        return if (x < 26) {
//            (s + (x + 97).toChar()).reversed();
//        } else {
//            val c = ((x % 26) + 97).toChar();
//            val r = (x / 26) - 1;
//            loop(s + c, r)
//        }
//    }
//
//    return loop("", this)
//}
//
//fun Environment.genSym(): String {
//    return this.size.toVar()
//}

fun randomLeaf(environment: Environment): Expression {
    return if (environment.isEmpty()) {
        Integer(Random.nextInt(1000))
    } else {
        if (Random.nextBoolean()) {
            Integer(Random.nextInt(1000))
        } else {
            environment.keys.toList()[Random.nextInt(environment.size)]
        }
    }
}

fun randomExpression(environment: Environment, maxDepth: Int): Expression {
    if (maxDepth == 0) {
        return randomLeaf(environment)
    } else {
        when (Random.nextInt(4)) {
            0 -> return randomLeaf(environment)
            1 -> return Negate(randomExpression(environment, maxDepth - 1))
            2 -> return Plus(
                randomExpression(environment, maxDepth - 1),
                randomExpression(environment, maxDepth - 1)
            )
            3 -> {
                val sym = listOf("a", "b", "c")[Random.nextInt(3)]
                //val sym = environment.genSym()
                val newEnv = environment.put(Var(sym), 0)
                return Let(sym, randomExpression(environment, maxDepth - 1), randomExpression(newEnv, maxDepth - 1))
            }
        }
        return Integer(0)
    }
}


fun main() {
    val p = Top(
        Let("x",
            Integer(10),
            Let("x",
                Var("x"),
                Var("x")
                )
        )
    )

    println("${p.interpret()}")

    val u = p.uniqueify()
    println(u)
    println("${u.interpret()}")
    println("${u.checkUniqueSymbols()}")
}