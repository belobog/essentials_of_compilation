package anf

import cvar.Block
import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.PersistentSet
import kotlinx.collections.immutable.persistentMapOf
import kotlinx.collections.immutable.persistentSetOf


typealias Environment = PersistentMap<Var, Int>

interface Program {
    fun interpret(environment: Environment): Int
}

interface Expression : Program {
    fun checkUniqueVars(): PersistentSet<Var>

    fun returnValue(): cvar.Expression

    /// Let is the only thing that does anything different for
    /// buildBlock, so provide a default
    fun buildBlock(cont: cvar.Block): cvar.Block {
        return cont
    }
}

interface Atomic : Expression {
    override fun returnValue(): cvar.Atomic
}


data class Integer(val value: Int) : Atomic {
    override fun interpret(environment: Environment): Int {
        return value
    }

    override fun checkUniqueVars(): PersistentSet<Var> {
        return persistentSetOf()
    }

    override fun returnValue(): cvar.Atomic {
        return cvar.Integer(value)
    }
}

object Read : Expression {
    override fun checkUniqueVars(): PersistentSet<Var> {
        return persistentSetOf()
    }

    override fun interpret(environment: Environment): Int {
        return readLine()!!.toInt()
    }

    override fun returnValue(): cvar.Expression {
        return cvar.Read
    }
}

data class Negate(val expression: Atomic) : Expression {
    override fun interpret(environment: Environment): Int {
        return 0 - expression.interpret(environment)
    }

    override fun checkUniqueVars(): PersistentSet<Var> {
        return expression.checkUniqueVars()
    }

    override fun returnValue(): cvar.Expression {
        return cvar.Negate(expression.returnValue())
    }
}

data class Plus(val left: Atomic, val right: Atomic) : Expression {
    override fun interpret(environment: Environment): Int {
        return left.interpret(environment) + right.interpret(environment)
    }

    override fun checkUniqueVars(): PersistentSet<Var> {
        val leftVars = left.checkUniqueVars()
        val rightVars = right.checkUniqueVars()

        assert(leftVars.intersect(rightVars).isEmpty())

        return leftVars.addAll(rightVars)
    }

    override fun returnValue(): cvar.Expression {
        return cvar.Plus(left.returnValue(), right.returnValue())
    }
}

data class Var(val name: String, val count: Int) : Atomic {
    override fun interpret(environment: Environment): Int {
        val v = environment[this]
        if (v == null) {
            error("Unable to find variable $this in environment $environment.")
        } else {
            return v
        }
    }

    override fun checkUniqueVars(): PersistentSet<Var> {
        return persistentSetOf()
    }

    override fun returnValue(): cvar.Atomic {
        return cvar.Var(name, count)
    }
}

data class Let(val boundVar: Var, val varExpression: Expression, val inExpression: Expression) : Expression {
    override fun interpret(environment: Environment): Int {
        val newEnv = environment.put(boundVar, varExpression.interpret(environment))
        return inExpression.interpret(newEnv)
    }

    override fun checkUniqueVars(): PersistentSet<Var> {
        val varVars = varExpression.checkUniqueVars()
        val inVars = inExpression.checkUniqueVars()

        val allVars = varVars.addAll(inVars)
        if (allVars.size != varVars.size + inVars.size) {
            println("allVars = $allVars")
            println("varVars = $varVars")
            println("inVars = $inVars")
        }


        assert(allVars.size == varVars.size + inVars.size)
        assert(boundVar !in allVars)

        return allVars.add(boundVar)
    }

    override fun returnValue(): cvar.Expression {
        return inExpression.returnValue()
    }

    override fun buildBlock(cont: Block): Block {
        val newCont = cvar.Sequence(
            cvar.Assign(cvar.Var(boundVar.name, boundVar.count), varExpression.returnValue()),
            inExpression.buildBlock(cont)
        )

        return varExpression.buildBlock(newCont)
    }
}

data class Top(val e: Expression) : Program {
    override fun interpret(environment: Environment): Int {
        return e.interpret(environment)
    }

    fun interpret(): Int {
        return e.interpret(persistentMapOf())
    }

    fun checkUniqueVars(): Boolean {
        val allVars = e.checkUniqueVars()
        return true
    }

    fun explicateControl() : cvar.Top {
        return cvar.Top(e.buildBlock(cvar.Return(e.returnValue())))
    }

}

fun main() {

    for (i in 0 until 1000000) {
        val p = rvar.Top.random(20);
//        println(p)
        val varValue = p.interpret()
        val a = p.uniqueify().removeComplex()
//        println(a)
        assert(a.checkUniqueVars())
        val anfValue = a.interpret()
        println("$i $varValue $anfValue")
        assert(varValue == anfValue)
    }
}